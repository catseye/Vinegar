# Copyright (c) 2021-2024 Chris Pressey, Cat's Eye Technologies.
# This file is distributed under a 2-clause BSD license.  For more info, see:
# SPDX-License-Identifier: LicenseRef-BSD-2-Clause-X-Vinegar

import sys

from vinegar.scanner import Scanner
from vinegar.parser import Parser
from vinegar.interpreter import interpret, BUILTINS


def main(args):
    program_text = sys.stdin.read()
    scanner = Scanner(program_text)
    parser = Parser(scanner, BUILTINS.copy())
    definitions = parser.program()
    # print(definitions['main'])
    s = interpret(definitions, [], definitions['main'])
    print(s)
